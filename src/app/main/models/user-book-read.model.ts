export interface BookRead {
  title: string;
  author: string;
  image: string;
  start: string;
  finish: string;
  rating: number;
  comment: string;
}
