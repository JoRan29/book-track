export class User {
  uid: string | undefined;
  name: string;
  email: string;

  constructor(uid: string = '', email: string = '', name: string = '') {
    this.uid = uid;
    this.email = email;
    this.name = name;
  }
}
