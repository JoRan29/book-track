export interface Book {
  title: string;
  author: string;
  writtenIn: string;
  image: string;
  addedBy: string | undefined;
  readBy: [];
}
