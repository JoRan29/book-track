import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainRoutingModule } from './main-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

import { UsersService } from './../auth/services/users.service';
import { BooksService } from './services/books.service';
import { UsersBooksService } from './services/users-books.service';

import { MainComponent } from './main.component';
import { BooksComponent } from './components/books/books.component';
import { AddBookComponent } from './components/add-book/add-book.component';
import { BooksDetailComponent } from './components/books-detail/books-detail.component';

import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCardModule } from '@angular/material/card';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

import { NgxMatFileInputModule } from '@angular-material-components/file-input';

import { UserBooksToReadComponent } from './components/user-books-to-read/user-books-to-read.component';
import { UserBooksReadComponent } from './components/user-books-read/user-books-read.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AddUserBookComponent } from './components/add-user-book/add-user-book.component';

@NgModule({
  declarations: [
    MainComponent,
    BooksComponent,
    AddBookComponent,
    BooksDetailComponent,
    UserBooksToReadComponent,
    UserBooksReadComponent,
    DashboardComponent,
    AddUserBookComponent,
  ],
  entryComponents: [AddBookComponent],
  providers: [UsersService, BooksService, UsersBooksService],
  imports: [
    CommonModule,
    MainRoutingModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule,
    MatDialogModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatAutocompleteModule,
    NgxMatFileInputModule,
  ],
})
export class MainModule {}
