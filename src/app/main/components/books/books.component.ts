import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Observable } from 'rxjs';

import { BooksService } from './../../services/books.service';
import { Book } from './../../models/book.model';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss'],
})
export class BooksComponent implements OnInit {
  books!: Observable<Book[]>;

  constructor(private booksService: BooksService, private router: Router) {}

  ngOnInit(): void {
    this.books = this.booksService.getAllBooks();
  }

  onSelectBook(book: any): void {
    this.router.navigate(['main/books', book.id]);
  }
}
