import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';

import { UsersBooksService } from './../../services/users-books.service';

import { AddUserBookComponent } from './../add-user-book/add-user-book.component';
import { Book } from './../../models/book.model';

import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-user-books-read',
  templateUrl: './user-books-read.component.html',
  styleUrls: ['./user-books-read.component.scss'],
})
export class UserBooksReadComponent implements OnInit {
  booksRead!: Observable<Book[]>;

  constructor(
    private userbooksService: UsersBooksService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.booksRead = this.userbooksService.getAllUsersBookRead();
  }

  openAddBookRead(): void {
    this.dialog.open(AddUserBookComponent, {
      minWidth: 500,
      minHeight: 600,
    });
  }
}
