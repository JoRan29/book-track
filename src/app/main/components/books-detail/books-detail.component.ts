import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { BooksService } from './../../services/books.service';
import { Book } from './../../models/book.model';

import { take } from 'rxjs/operators';

@Component({
  selector: 'app-books-detail',
  templateUrl: './books-detail.component.html',
  styleUrls: ['./books-detail.component.scss'],
})
export class BooksDetailComponent implements OnInit {
  bookId!: string | null;
  book!: Book;

  constructor(
    private activeRoute: ActivatedRoute,
    private booksService: BooksService
  ) {}

  ngOnInit(): void {
    this.bookId = this.activeRoute.snapshot.paramMap.get('id');

    if (this.bookId) {
      this.booksService
        .getBookById(this.bookId)
        .pipe(take(1))
        .subscribe((book) => (this.book = book));
    }
  }
}
