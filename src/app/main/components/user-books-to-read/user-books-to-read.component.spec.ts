import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserBooksToReadComponent } from './user-books-to-read.component';

describe('UserBooksToReadComponent', () => {
  let component: UserBooksToReadComponent;
  let fixture: ComponentFixture<UserBooksToReadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserBooksToReadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserBooksToReadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
