import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';

import { UsersBooksService } from './../../services/users-books.service';

import { Book } from './../../models/book.model';

@Component({
  selector: 'app-user-books-to-read',
  templateUrl: './user-books-to-read.component.html',
  styleUrls: ['./user-books-to-read.component.scss'],
})
export class UserBooksToReadComponent implements OnInit {
  booksToRead!: Observable<Book[]>;

  constructor(private userbooksService: UsersBooksService) {}

  ngOnInit(): void {
    this.booksToRead = this.userbooksService.getAllUsersBookToRead();
    console.log(this.booksToRead);
  }
}
