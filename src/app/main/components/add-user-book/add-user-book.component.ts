import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Observable } from 'rxjs';
import { startWith, map, take, debounceTime } from 'rxjs/operators';

import { UsersBooksService } from './../../services/users-books.service';
import { BooksService } from './../../services/books.service';
import { Book } from './../../models/book.model';

import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-add-user-book',
  templateUrl: './add-user-book.component.html',
  styleUrls: ['./add-user-book.component.scss'],
})
export class AddUserBookComponent implements OnInit {
  addUserBookForm!: FormGroup;
  submitted = false;
  allBooks!: Book[];
  filteredBooks!: Observable<Book[]>;

  constructor(
    private fb: FormBuilder,
    private usersBooksService: UsersBooksService,
    private booksService: BooksService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.buildForm();
    this.booksService
      .getAllBooks()
      .pipe(take(1))
      .subscribe((data) => {
        this.allBooks = data;
      });
    this.filteredBooks = this.addUserBookForm.get('title')?.valueChanges.pipe(
      startWith(''),
      map((value) => (typeof value === 'string' ? value : value.title)),
      map((title) => (title ? this._filter(title) : this.allBooks)),
      debounceTime(400)
    );
  }

  private buildForm(): void {
    this.addUserBookForm = this.fb.group({
      title: ['', [Validators.required]],
      started: ['', [Validators.required]],
      finished: ['', [Validators.required]],
    });
  }

  submitForm(): void {
    if (this.addUserBookForm.valid) {
      console.log(this.addUserBookForm.value);
      this.usersBooksService.addReadBook(this.addUserBookForm.value);
      this.submitted = true;
      this.openSnackBar('Book Added to Read!');
    }
  }

  displayFn(book: Book): string {
    return book && book.title ? book.title : '';
  }

  _filter(title: string): Book[] {
    const filterValue = title.toLowerCase();

    const result = this.allBooks.filter(
      (option) => option.title.toLowerCase().indexOf(filterValue) === 0
    );

    return result;
  }

  openSnackBar(message: string): void {
    this.snackBar.open(message, undefined, {
      duration: 2000,
    });
  }
}
