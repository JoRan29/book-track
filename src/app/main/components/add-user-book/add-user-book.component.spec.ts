import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUserBookComponent } from './add-user-book.component';

describe('AddUserBookComponent', () => {
  let component: AddUserBookComponent;
  let fixture: ComponentFixture<AddUserBookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddUserBookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUserBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
