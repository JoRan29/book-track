import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { MatSnackBar } from '@angular/material/snack-bar';

import { BooksService } from './../../services/books.service';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.scss'],
})
export class AddBookComponent implements OnInit {
  addBookForm!: FormGroup;
  submitted = false;
  currentUser!: string;

  constructor(
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private booksService: BooksService
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }

  private buildForm(): void {
    this.addBookForm = this.fb.group({
      title: [
        '',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(25),
        ],
      ],
      author: [
        '',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(25),
        ],
      ],
      image: [''],
      writtenIn: ['', [Validators.required]],
    });
  }

  submitForm(): void {
    if (this.addBookForm.valid) {
      this.submitted = true;
      this.booksService.uploadBook(this.addBookForm.value);
      this.openSnackBar('Book Added to the Global Database!');
    }
  }

  openSnackBar(message: string): void {
    this.snackBar.open(message, undefined, {
      duration: 2000,
    });
  }
}
