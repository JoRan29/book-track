import { Component, OnInit } from '@angular/core';

import { AddBookComponent } from './../add-book/add-book.component';

import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  constructor(public dialog: MatDialog) {}

  ngOnInit(): void {}

  openAddBook(): void {
    this.dialog.open(AddBookComponent, {
      minWidth: 500,
      minHeight: 600,
    });
  }
}
