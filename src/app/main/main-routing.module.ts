import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainComponent } from './main.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { BooksComponent } from './components/books/books.component';
import { AddBookComponent } from './components/add-book/add-book.component';
import { BooksDetailComponent } from './components/books-detail/books-detail.component';
import { UserBooksReadComponent } from './components/user-books-read/user-books-read.component';
import { UserBooksToReadComponent } from './components/user-books-to-read/user-books-to-read.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/main/dashboard',
    pathMatch: 'full',
  },
  {
    path: '',
    component: MainComponent,

    children: [
      {
        path: 'dashboard',
        component: DashboardComponent,
      },
      {
        path: 'books',
        component: BooksComponent,
      },
      {
        path: 'add-book',
        component: AddBookComponent,
      },
      {
        path: 'books/:id',
        component: BooksDetailComponent,
      },
      {
        path: 'books-read',
        component: UserBooksReadComponent,
      },
      {
        path: 'books-to-read',
        component: UserBooksToReadComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainRoutingModule {}
