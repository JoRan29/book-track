import { Injectable } from '@angular/core';

import {
  AngularFirestore,
  AngularFirestoreCollection,
  DocumentReference,
} from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFireAuth } from '@angular/fire/auth';

import { Book } from '../models/book.model';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class BooksService {
  private bookCollection: AngularFirestoreCollection<Book>;
  books!: Observable<Book[]>;
  book!: Book;
  downloadURL!: Observable<string>;

  constructor(
    private firestore: AngularFirestore,
    private readonly firestorage: AngularFireStorage,
    private auth: AngularFireAuth
  ) {
    this.bookCollection = firestore.collection('books');
  }

  getAllBooks(): Observable<Book[]> {
    return this.bookCollection.snapshotChanges().pipe(
      map((actions) =>
        actions.map((a) => {
          const data = a.payload.doc.data() as Book;
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      )
    );
  }

  getBookById(id: string): Observable<Book> {
    return this.bookCollection
      .doc(id)
      .snapshotChanges()
      .pipe(
        map((actions) => {
          const data = actions.payload.data() as Book;
          return data;
        })
      );
  }

  uploadBook(form: any): void {
    let userUid;
    this.auth.authState.subscribe((currentUser) => {
      userUid = currentUser?.uid;
      this.book = {
        title: form.title,
        author: form.author,
        writtenIn: form.writtenIn,
        image: '',
        addedBy: userUid,
        readBy: [],
      };
    });

    const file = form.image;
    const filePath = `bookCovers/${form.title.replace(/\s/g, '')}`;
    this.uploadImage(file, filePath);
  }

  saveBook(book: Book): Promise<DocumentReference<unknown>> {
    return this.firestore.collection('books').add(book);
  }

  uploadImage(file: HTMLElement, filePath: string): void {
    const fileRef = this.firestorage.ref(filePath);
    const task = this.firestorage.upload(filePath, file);
    task
      .snapshotChanges()
      .pipe(
        finalize(() => {
          this.downloadURL = fileRef.getDownloadURL();
          this.downloadURL.subscribe((url) => {
            if (url) {
              this.book.image = url;
              this.saveBook(this.book);
            }
          });
        })
      )
      .subscribe((url) => {
        if (url) {
          console.log(url);
        }
      });
  }

  deleteBook(id: string, title: string): Promise<void> {
    this.deleteImagebyRef(title.replace(/\s/g, ''));
    return this.firestore.collection('books').doc(id).delete();
  }

  deleteImagebyRef(filePath: string): void {
    this.firestorage.ref(filePath).delete();
  }
}
