import { Injectable } from '@angular/core';

import {
  AngularFirestore,
  AngularFirestoreCollection,
  DocumentReference,
} from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFireAuth } from '@angular/fire/auth';

import { Book } from '../models/book.model';
import { BookRead } from './../models/user-book-read.model';

import { Observable, EMPTY } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class UsersBooksService {
  private usersBookCollectionRead!: AngularFirestoreCollection<BookRead>;
  private usersBookCollectionToRead!: AngularFirestoreCollection<Book>;

  constructor(
    private firestore: AngularFirestore,
    private readonly firestorage: AngularFireStorage,
    private auth: AngularFireAuth
  ) {}

  getUsersBookReadColl(id: string): AngularFirestoreCollection<BookRead> {
    this.usersBookCollectionRead = this.firestore
      .collection('users')
      .doc(id)
      .collection('read');
    return this.usersBookCollectionRead;
  }

  getUsersBookToReadColl(
    id: string | undefined
  ): AngularFirestoreCollection<Book> {
    this.usersBookCollectionToRead = this.firestore
      .collection('users')
      .doc(id)
      .collection('toRead');
    return this.usersBookCollectionToRead;
  }

  getAllUsersBookToRead(): Observable<Book[]> {
    const loggedUser = localStorage.getItem('currentUser');
    if (loggedUser) {
      return this.firestore
        .collection('users')
        .doc(loggedUser)
        .collection('toRead')
        .snapshotChanges()
        .pipe(
          map((actions) =>
            actions.map((a) => {
              const data = a.payload.doc.data() as Book;
              const id = a.payload.doc.id;
              return { id, ...data };
            })
          )
        );
    }
    return EMPTY;
  }

  getAllUsersBookRead(): Observable<Book[]> {
    const loggedUser = localStorage.getItem('currentUser');
    if (loggedUser) {
      return this.firestore
        .collection('users')
        .doc(loggedUser)
        .collection('read')
        .snapshotChanges()
        .pipe(
          map((actions) =>
            actions.map((a) => {
              const data = a.payload.doc.data() as Book;
              const id = a.payload.doc.id;
              return { id, ...data };
            })
          )
        );
    }
    return EMPTY;
  }

  addReadBook(book: BookRead): Promise<DocumentReference<unknown>> {
    const loggedUser = localStorage.getItem('currentUser');
    if (loggedUser) {
      return this.firestore
        .collection('users')
        .doc(loggedUser)
        .collection('read')
        .add(book);
    }
  }
}
