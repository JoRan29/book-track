import { Component } from '@angular/core';
import { Router, RouterOutlet } from '@angular/router';

import { routerAnimation } from './animations/animations';

import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [routerAnimation],
})
export class AppComponent {
  constructor(private auth: AngularFireAuth, private router: Router) {}

  isLoggedIn(): boolean {
    let logged = false;
    if (localStorage.getItem('currentUser')) {
      logged = true;
    }
    return logged;
  }

  getState(outlet: RouterOutlet): void {
    return (
      outlet && outlet.activatedRouteData && outlet.activatedRouteData.state
    );
  }

  onLogout(): void {
    this.auth.signOut().then(() => {
      localStorage.removeItem('currentUser');
      this.router.navigate(['']);
    });
  }
}
