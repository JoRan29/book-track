import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';

import { UsersService } from './../../services/users.service';

import { MatSnackBar } from '@angular/material/snack-bar';

import { slideDown } from './../../../animations/animations';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [slideDown],
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;
  submitted = false;
  error = '';
  hidePassword = true;

  constructor(
    private fb: FormBuilder,
    private auth: AngularFireAuth,
    private usersService: UsersService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }

  private buildForm(): void {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
    });
  }

  submitForm(): void {
    if (this.loginForm.valid) {
      const { email, password } = this.loginForm.value;
      this.auth
        .signInWithEmailAndPassword(email, password)
        .then(() => {
          this.usersService.setCurrentUserUidLS();
          this.router.navigateByUrl('/main');
          this.submitted = true;
          this.openSnackBar('Successful Login!');
        })
        .catch((error) => {
          let errorMessage = `An error occured - Please, try again!`;
          if (error.message) {
            errorMessage = `${error.message}`;
          }
          this.openSnackBar(`${errorMessage}`);
        });
    }
  }

  openSnackBar(message: string): void {
    this.snackBar.open(message, undefined, {
      duration: 2000,
    });
  }
}
