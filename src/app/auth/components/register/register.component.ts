import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AngularFireAuth } from '@angular/fire/auth';
import { UsersService } from './../../services/users.service';
import { User } from './../../../main/models/user.model';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { passwordPattern } from './../../../app.constants';

import { MatSnackBar } from '@angular/material/snack-bar';

import { slideDown } from './../../../animations/animations';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  animations: [slideDown],
})
export class RegisterComponent implements OnInit {
  registerForm!: FormGroup;
  submitted = false;
  error = '';
  hidePassword = true;

  constructor(
    private fb: FormBuilder,
    private auth: AngularFireAuth,
    private usersService: UsersService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }

  private buildForm(): void {
    this.registerForm = this.fb.group({
      name: [
        '',
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(15),
        ],
      ],
      email: ['', [Validators.required, Validators.email]],
      password: [
        '',
        [Validators.required, Validators.pattern(passwordPattern)],
      ],
    });
  }

  submitForm(): void {
    if (this.registerForm.valid) {
      const { name, email, password } = this.registerForm.value;
      this.auth
        .createUserWithEmailAndPassword(email, password)
        .then((res) => {
          const newUser: User = {
            uid: res.user?.uid,
            email,
            name,
          };
          this.usersService.saveUser$(newUser);
          this.usersService.setCurrentUserUidLS();
          this.router.navigateByUrl('/main');
          this.submitted = true;
          this.openSnackBar('Registration complete!');
        })
        .catch((error) => {
          let errorMessage = `An error occured - Please, try again!`;
          if (error.message) {
            errorMessage = `${error.message}`;
          }
          this.openSnackBar(`${errorMessage}`);
        });
    }
  }

  openSnackBar(message: string): void {
    this.snackBar.open(message, undefined, {
      duration: 2000,
    });
  }
}
