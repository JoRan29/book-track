import { Injectable } from '@angular/core';
import { User } from './../../main/models/user.model';

import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  users!: User[];

  constructor(
    private firestore: AngularFirestore,
    private auth: AngularFireAuth
  ) {}

  getCurrentUserUid(): string | null {
    return localStorage.getItem('currentUser');
  }

  setCurrentUserUidLS(): void {
    this.auth.authState.subscribe((currentUser) => {
      if (currentUser) {
        localStorage.setItem('currentUser', currentUser?.uid);
      }
    });
  }

  saveUser$(user: User): Promise<void> {
    return this.firestore.collection('users').doc(user.uid).set(user);
  }
}
