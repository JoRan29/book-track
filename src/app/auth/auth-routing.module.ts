import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthComponent } from './auth.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';

import { canActivate } from '@angular/fire/auth-guard';
import { redirectLoggedInTo } from '@angular/fire/auth-guard';
const redirectLoggedInToItems = () => redirectLoggedInTo(['main']);

const routes: Routes = [
  {
    path: '',
    redirectTo: '/auth',
    pathMatch: 'full',
    ...canActivate(redirectLoggedInToItems),
  },
  {
    path: '',
    component: AuthComponent,
    ...canActivate(redirectLoggedInToItems),

    children: [
      {
        path: 'login',
        component: LoginComponent,
        ...canActivate(redirectLoggedInToItems),
      },
      {
        path: 'register',
        component: RegisterComponent,
        ...canActivate(redirectLoggedInToItems),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {}
