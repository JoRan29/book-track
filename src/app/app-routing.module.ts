import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { canActivate, redirectUnauthorizedTo } from '@angular/fire/auth-guard';
const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['auth']);

import { AboutComponent } from './nav/components/about/about.component';
import { ContactComponent } from './nav/components/contact/contact.component';
import { NotFoundComponent } from './nav/components/not-found/not-found.component';

const routes: Routes = [
  { path: '', redirectTo: '/auth', pathMatch: 'full' },
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then((m) => m.AuthModule),
    data: { state: 'auth' },
  },
  {
    path: 'main',
    loadChildren: () => import('./main/main.module').then((m) => m.MainModule),
    ...canActivate(redirectUnauthorizedToLogin),
    data: { state: 'main' },
  },
  {
    path: 'about',
    component: AboutComponent,
    data: { state: 'about' },
  },
  {
    path: 'contact',
    component: ContactComponent,
    data: { state: 'contact' },
  },
  { path: '404', component: NotFoundComponent, data: { state: '404' } },
  { path: '**', redirectTo: '/404', data: { state: 'wildpath' } },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
